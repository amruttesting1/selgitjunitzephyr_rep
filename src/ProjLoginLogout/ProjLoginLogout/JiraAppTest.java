package ProjLoginLogout.ProjLoginLogout;

import java.io.IOException;
import java.util.List;

import javax.enterprise.inject.New;

import org.apache.xmlbeans.impl.piccolo.io.FileFormatException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;


public class JiraAppTest {
	LoginLogout j = new LoginLogout();
	WebDriver driver=InitializeBrowser.testdriver();
	//FullTestCase F= new FullTestCase();
	
  @Test
  public void JiraAppLogin() throws FileFormatException, IOException, InterruptedException
  {		 	 
	  System.out.println("Testing Login ..");
  
  			j.login();
  
  }

  @Test
  public void JiraApplogout() throws FileFormatException, IOException
  {	
	 j.logout();
	 System.out.println("Testing Logout ...");
  }
  
  //@Test(priority =1)
  public void CreateUser() throws FileFormatException, IOException
  {		 	j.createusers();
	  		//System.out.println("Testing Login and logout..");
  }

  @Before
  public void beforeMethod() throws InterruptedException {
	 j.TestStartTime();
	//driver=F.chromeBrowser();
	  }
  @After
  public void afterMethod() {
	  
	  j.TestEndTime();
	//driver.quit();
  }

}

